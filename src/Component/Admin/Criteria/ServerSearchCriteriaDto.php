<?php

declare(strict_types = 1);


namespace App\Component\Admin\Criteria;


use Ramsey\Uuid\UuidInterface;

class ServerSearchCriteriaDto
{
    /**
     * @var UuidInterface[]
     */
    public $ids;

    /**
     * @var UuidInterface
     */
    public $ownerId;
}