<?php

declare(strict_types = 1);


namespace App\Component\Admin\Criteria;


use App\Component\Job\Enum\JobStatus;
use League\Period\Period;
use Ramsey\Uuid\UuidInterface;

class JobSearchCriteriaDto
{
    /**
     * @var UuidInterface|null
     */
    public $serverId;

    /**
     * @var Period|null
     */
    public $period;

    /**
     * @var JobStatus|null
     */
    public $status;
}