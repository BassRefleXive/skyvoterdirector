<?php

declare(strict_types = 1);


namespace App\Component\Admin\Criteria\Factory;


use App\Component\Admin\Criteria\JobSearchCriteriaDto;
use App\Component\Job\Enum\JobStatus;
use FOS\RestBundle\Request\ParamFetcherInterface;
use League\Period\Period;
use Ramsey\Uuid\Uuid;

class JobSearchCriteriaDtoFactory
{
    public function fromParams(ParamFetcherInterface $params): JobSearchCriteriaDto
    {
        $dto = new JobSearchCriteriaDto();

        $params = $params->all();

        if (isset($params['server_id'])) {
            $dto->serverId = Uuid::fromString($params['server_id']);
        }

        if (isset($params['start_date']) && isset($params['end_date'])) {
            $dto->period = new Period(new \DateTime($params['start_date']), new \DateTime($params['end_date']));
        }

        if (isset($params['status'])) {
            $dto->status = JobStatus::byName($params['status']);
        }

        return $dto;
    }
}