<?php

declare(strict_types = 1);


namespace App\Component\Admin\Criteria\Factory;


use App\Component\Admin\Criteria\ServerSearchCriteriaDto;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\Uuid;

class ServerSearchCriteriaDtoFactory
{
    public function fromParams(ParamFetcherInterface $params): ServerSearchCriteriaDto
    {
        $dto = new ServerSearchCriteriaDto();

        $params = $params->all();

        if (isset($params['ids'])) {
            $dto->ids = array_map([Uuid::class, 'fromString'], $params['ids']);
        }

        if (isset($params['owner_id'])) {
            $dto->ownerId = Uuid::fromString($params['owner_id']);
        }

        return $dto;
    }
}