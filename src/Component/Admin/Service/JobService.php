<?php

declare(strict_types=1);


namespace App\Component\Admin\Service;


use App\Component\Admin\Criteria\JobSearchCriteriaDto;
use App\Component\Job\Enum\JobStatus;
use App\Component\Job\Model\Builder\JobBuilder;
use App\Component\Job\Model\Job;
use App\Component\Job\Repository\JobRepositoryInterface;
use App\Component\Top\Repository\ServerRepositoryInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;
use League\Period\Period;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class JobService
{
    private $jobRepository;
    private $serverRepository;

    public function __construct(JobRepositoryInterface $jobRepository, ServerRepositoryInterface $serverRepository)
    {
        $this->jobRepository = $jobRepository;
        $this->serverRepository = $serverRepository;
    }

    public function create(ParamFetcherInterface $params): array
    {
        $jobsData = $params->get('jobs');

        $jobs = [];

        foreach ($jobsData as $jobData) {
            $jobs[] = (new JobBuilder($this->jobRepository->nextIdentity()))
                ->withServer($this->serverRepository->getById(Uuid::fromString($params->get('server_id'))))
                ->withDate(new \DateTime($jobData['date']))
                ->withStatus(JobStatus::byValue(JobStatus::ENABLED))
                ->withOrderedVotes((int) $jobData['votes'])
                ->withPendingVotes(0)
                ->withExecutedVotes(0)
                ->withFailedVotes(0)
                ->build();
        }

        $this->jobRepository->bulkSave($jobs);

        return $jobs;
    }

    public function update(UuidInterface $id, ParamFetcherInterface $params): Job
    {
        /** @var Job $job */
        $job = $this->jobRepository->getById($id);

        $params = $params->all();

        isset($params['ordered_votes']) && $job->changeOrderedVotes((int) $params['ordered_votes']);
        isset($params['pending_votes']) && $job->changePendingVotes((int) $params['pending_votes']);
        isset($params['executed_votes']) && $job->changeExecutedVotes((int) $params['executed_votes']);


        if (isset($params['status'])) {
            $status = JobStatus::byName($params['status']);

            switch (true) {
                case $status->isEnabled() && !$job->status()->isEnabled():
                    {
                        $job->enable();

                        break;
                    }
                case $status->isPaused() && !$job->status()->isPaused():
                    {
                        $job->pause();

                        break;
                    }
                case $status->isDisabled() && !$job->status()->isDisabled():
                    {
                        $job->disable();

                        break;
                    }
            }
        }

        $this->jobRepository->save($job);

        return $job;
    }

    public function duplicate(Period $period, \DateTimeInterface $startDate): array
    {
        $criteria = new JobSearchCriteriaDto();
        $criteria->period = $period;

        $jobs = $this->jobRepository->findByCriteria($criteria);

        $new = [];

        foreach ($jobs as $job) {
            $new[] = (new JobBuilder($this->jobRepository->nextIdentity()))
                ->withServer($job->server())
                ->withDate(
                    (new \DateTime())
                        ->setTimestamp($startDate->getTimestamp())
                        ->add(
                            new \DateInterval(
                                sprintf(
                                    'P%dD',
                                    $job->date()->diff($period->getStartDate())->d
                                )
                            )
                        )
                )
                ->withStatus(JobStatus::byValue(JobStatus::ENABLED))
                ->withOrderedVotes($job->orderedVotes())
                ->withPendingVotes(0)
                ->withExecutedVotes(0)
                ->withFailedVotes(0)
                ->build();
        }

        $this->jobRepository->bulkSave($new);

        return $new;
    }

    public function changeOrderedVotesCount(Period $period, float $multiplier, UuidInterface $serverId = null): array
    {
        $criteria = new JobSearchCriteriaDto();
        $criteria->period = $period;
        $criteria->serverId = $serverId;

        $jobs = $this->jobRepository->findByCriteria($criteria);

        foreach ($jobs as $job) {
            $job->changeOrderedVotes(
                (int) round($job->orderedVotes() * $multiplier)
            );
        }

        $this->jobRepository->bulkSave($jobs);

        return $jobs;
    }
}