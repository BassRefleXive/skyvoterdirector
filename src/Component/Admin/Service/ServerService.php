<?php

declare(strict_types=1);

namespace App\Component\Admin\Service;


use App\Component\Top\Enum\ServerStatus;
use App\Component\Top\Model\Server;
use App\Component\Top\Repository\ServerRepositoryInterface;
use App\Component\Top\Repository\TopRepositoryInterface;
use App\Component\User\Repository\UserRepositoryInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ServerService
{
    private $serverRepository;
    private $topRepository;
    private $userRepository;

    public function __construct(ServerRepositoryInterface $serverRepository, TopRepositoryInterface $topRepository, UserRepositoryInterface $userRepository)
    {
        $this->serverRepository = $serverRepository;
        $this->topRepository = $topRepository;
        $this->userRepository = $userRepository;
    }

    public function create(ParamFetcherInterface $params): Server
    {
        $server = new Server(
            $this->serverRepository->nextIdentity(),
            $this->userRepository->getById(Uuid::fromString($params->get('owner_id'))),
            $this->topRepository->getById(Uuid::fromString($params->get('top_id'))),
            $params->get('provider_id'),
            $params->get('title')
        );

        $this->serverRepository->save($server);

        return $server;
    }

    public function update(UuidInterface $id, ParamFetcherInterface $params): Server
    {
        /** @var Server $server */
        $server = $this->serverRepository->getById($id);

        $params = $params->all();

        if (isset($params['status'])) {
            $status = ServerStatus::byName($params['status']);

            switch (true) {
                case $status->isEnabled(): {
                    $server->enable();

                    break;
                }
                case $status->isPaused(): {
                    $server->pause();

                    break;
                }
                case $status->isDisabled(): {
                    $server->disable();

                    break;
                }
            }
        }

        $this->serverRepository->save($server);

        return $server;
    }
}