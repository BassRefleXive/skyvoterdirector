<?php

declare(strict_types=1);

namespace App\Component\Admin\Service;


use App\Component\CaptchaResolver\Factory\CaptchaResolverFactory;
use App\Component\CaptchaResolver\Model\CaptchaResolver;
use App\Component\CaptchaResolver\Repository\CaptchaResolverRepositoryInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\UuidInterface;

class CaptchaResolverService
{
    private $captchaResolverRepository;

    public function __construct(CaptchaResolverRepositoryInterface $captchaResolverRepository)
    {
        $this->captchaResolverRepository = $captchaResolverRepository;
    }

    public function create(ParamFetcherInterface $params): CaptchaResolver
    {
        $captchaResolver = (new CaptchaResolverFactory())->fromParams($this->captchaResolverRepository->nextIdentity(), $params);

        $this->captchaResolverRepository->save($captchaResolver);

        return $captchaResolver;
    }

    public function update(UuidInterface $id, ParamFetcherInterface $params): CaptchaResolver
    {
        /** @var CaptchaResolver $top */
        $top = $this->captchaResolverRepository->getById($id);

        $params = $params->all();

        isset($params['code']) && $top->changeCode($params['code']);
        isset($params['key']) && $top->changeKey($params['key']);

        $this->captchaResolverRepository->save($top);

        return $top;
    }
}