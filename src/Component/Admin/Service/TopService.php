<?php

declare(strict_types=1);

namespace App\Component\Admin\Service;


use App\Component\CaptchaResolver\Repository\CaptchaResolverRepositoryInterface;
use App\Component\Top\Enum\TopStatus;
use App\Component\Top\Model\Builder\TopBuilder;
use App\Component\Top\Model\Top;
use App\Component\Top\Repository\TopRepositoryInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class TopService
{
    private $topRepository;
    private $captchaResolverRepository;

    public function __construct(TopRepositoryInterface $topRepository, CaptchaResolverRepositoryInterface $captchaResolverRepository)
    {
        $this->topRepository = $topRepository;
        $this->captchaResolverRepository = $captchaResolverRepository;
    }

    public function create(ParamFetcherInterface $params): Top
    {
        $top = (new TopBuilder())
            ->withId($this->topRepository->nextIdentity())
            ->withTitle($params->get('title'))
            ->withCode($params->get('code'))
            ->withMultiplier((float) $params->get('multiplier'))
            ->withStatus(TopStatus::byValue(TopStatus::DISABLED))
            ->withCaptchaResolver($this->captchaResolverRepository->getById(Uuid::fromString($params->get('captcha_resolver'))))
            ->build();

        $this->topRepository->save($top);

        return $top;
    }

    public function update(UuidInterface $id, ParamFetcherInterface $params): Top
    {
        /** @var Top $top */
        $top = $this->topRepository->getById($id);

        $params = $params->all();

        isset($params['title']) && $top->changeTitle($params['title']);
        isset($params['code']) && $top->changeCode($params['code']);
        isset($params['multiplier']) && $top->changeMultiplier((float) $params['multiplier']);
        isset($params['status']) && $top->changeStatus(TopStatus::byName($params['status']));
        isset($params['captcha_resolver']) && $top->changeCaptchaResolver($this->captchaResolverRepository->getById(Uuid::fromString($params['captcha_resolver'])));

        $this->topRepository->save($top);

        return $top;
    }
}