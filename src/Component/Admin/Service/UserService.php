<?php

declare(strict_types=1);

namespace App\Component\Admin\Service;


use App\Component\User\Enum\Status;
use App\Component\User\Factory\UserFactory;
use App\Component\User\Model\User;
use App\Component\User\Repository\UserRepositoryInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\UuidInterface;

class UserService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(ParamFetcherInterface $params): User
    {
        $user = (new UserFactory())->fromParams($this->userRepository->nextIdentity(), $params);

        $this->userRepository->save($user);

        return $user;
    }

    public function update(UuidInterface $id, ParamFetcherInterface $params): User
    {
        /** @var User $user */
        $user = $this->userRepository->getById($id);

        $params = $params->all();

        if (isset($params['status'])) {
            Status::byName($params['status'])->isActive()
                ? $user->unban()
                : $user->ban();
        }

        $this->userRepository->save($user);

        return $user;
    }
}