<?php

declare(strict_types=1);

namespace App\Component\User\Enum\Doctrine;

use App\Component\User\Enum\UserType as UserTypeEnum;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class UserType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?UserTypeEnum
    {
        return null !== $value
            ? UserTypeEnum::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        if (null === $value) {
            return null;
        }

        if ($value instanceof UserTypeEnum) {
            return $value->getValue();
        }

        return (int) $value;
    }

    public function getName(): string
    {
        return 'user_type';
    }
}