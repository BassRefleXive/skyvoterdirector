<?php

declare(strict_types=1);

namespace App\Component\User\Enum\Doctrine;

use App\Component\User\Enum\Status;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class StatusType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Status
    {
        return null !== $value
            ? Status::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var Status $value */
        return null !== $value
            ? (string) $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'user_status_type';
    }
}