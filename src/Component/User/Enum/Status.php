<?php

declare(strict_types=1);

namespace App\Component\User\Enum;


use MabeEnum\Enum;

final class Status extends Enum
{
    public const ACTIVE = 1;
    public const BANNED = 0;

    public function isActive(): bool
    {
        return $this->is(self::ACTIVE);
    }

    public function isBanned(): bool
    {
        return $this->is(self::BANNED);
    }
}