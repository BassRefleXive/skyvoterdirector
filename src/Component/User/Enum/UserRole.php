<?php

declare(strict_types=1);

namespace App\Component\User\Enum;

use MabeEnum\Enum;

final class UserRole extends Enum
{
    public const ROLE_CLIENT = 'ROLE_CLIENT';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
}