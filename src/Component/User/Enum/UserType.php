<?php

declare(strict_types=1);

namespace App\Component\User\Enum;

use MabeEnum\Enum;

final class UserType extends Enum
{
    public const CLIENT = 0;
    public const ADMIN = 1;

    public function isClient(): bool
    {
        return $this->is(self::CLIENT);
    }

    public function isAdmin(): bool
    {
        return $this->is(self::ADMIN);
    }
}