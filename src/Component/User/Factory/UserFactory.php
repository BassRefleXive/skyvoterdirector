<?php

declare(strict_types=1);

namespace App\Component\User\Factory;


use App\Component\User\Enum\UserType;
use App\Component\User\Model\Admin;
use App\Component\User\Model\Client;
use App\Component\User\Model\User;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\UuidInterface;

class UserFactory
{
    public function fromParams(UuidInterface $id, ParamFetcherInterface $params): User
    {
        $type = UserType::byName($params->get('type'));

        $user = $type->isAdmin()
            ? new Admin($id, $params->get('email'))
            : new Client($id, $params->get('email'));

        $user->changePassword($params->get('password'));

        return $user;
    }
}