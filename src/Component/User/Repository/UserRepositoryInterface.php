<?php

declare(strict_types=1);

namespace App\Component\User\Repository;


use App\Component\Core\Repository\RepositoryInterface;
use App\Component\User\Model\User;
use Ramsey\Uuid\UuidInterface;

interface UserRepositoryInterface extends RepositoryInterface
{
    public function getByEmail(string $email): User;
}