<?php

declare(strict_types=1);

namespace App\Component\User\Repository\Doctrine;

use App\Component\Core\Repository\Doctrine\BaseRepository;
use App\Component\User\Exception\UserNotFoundException;
use App\Component\User\Model\User;
use App\Component\User\Repository\UserRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getByEmail(string $email): User
    {
        if (null === $user = $this->findOneBy(['email' => $email])) {
            throw UserNotFoundException::missingEmail($email);
        }

        return $user;
    }
}