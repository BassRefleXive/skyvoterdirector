<?php

declare(strict_types=1);

namespace App\Component\User\Service;


use App\Component\User\Exception\UserNotFoundException;
use App\Component\User\Model\User;
use App\Component\User\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    private $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function loadUserByUsername($username)
    {
        try {
            $user = $this->repository->getByEmail($username);
        } catch (UserNotFoundException $e) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        } catch (EntityNotFoundException $e) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $id = $user->id();

        try {
            return $this->repository->getById($user->id());
        } catch (UserNotFoundException $e) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', $id));
        } catch (EntityNotFoundException $e) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', $id));
        }
    }

    public function supportsClass($class)
    {
        return $class === User::class;
    }
}