<?php

declare(strict_types=1);

namespace App\Component\User\Exception;


use Ramsey\Uuid\UuidInterface;

class UserNotFoundException extends \RuntimeException
{
    public static function missingEmail(string $email): self
    {
        return new self(sprintf('User with email "%s" not found.', $email));
    }

    public static function missingId(UuidInterface $id): self
    {
        return new self(sprintf('User with id "%s" not found.', $id));
    }
}