<?php

declare(strict_types=1);

namespace App\Component\User\Model;

use App\Component\User\Enum\Status;
use App\Component\User\Enum\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class User implements UserInterface
{
    protected const ROLES = [];

    private $id;
    private $email;
    private $password;
    private $status;
    private $servers;

    public function __construct(UuidInterface $uuid, string $email)
    {
        $this->id = $uuid;
        $this->email = $email;
        $this->status = Status::byValue(Status::ACTIVE);
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function changePassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function ban(): void
    {
        if ($this->status()->isBanned()) {
            throw new \LogicException(sprintf('User "%s" already banned.', $this->id()));
        }

        $this->status = Status::byValue(Status::BANNED);
    }

    public function unban(): void
    {
        if (!$this->status()->isBanned()) {
            throw new \LogicException(sprintf('User "%s" is not banned.', $this->id()));
        }

        $this->status = Status::byValue(Status::ACTIVE);
    }

    public function servers(): Collection
    {
        null === $this->servers && $this->servers = new ArrayCollection();

        return $this->servers;
    }

    abstract public function type(): UserType;




    /**==================================================================================================================**/
    /**
     * @deprecated Following method is needed only for integration with Symfony, Do not use in app.
     */
    public function getRoles(): array
    {
        return static::ROLES;
    }

    /**
     * @deprecated Following method is needed only for integration with Symfony, Do not use in app.
     */
    public function getPassword(): string
    {
        return $this->password();
    }

    /**
     * @deprecated Following method is needed only for integration with Symfony, Do not use in app.
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @deprecated Following method is needed only for integration with Symfony, Do not use in app.
     */
    public function getUsername(): string
    {
        return $this->email();
    }

    /**
     * @deprecated Following method is needed only for integration with Symfony, Do not use in app.
     */
    public function eraseCredentials(): void
    {
    }
}