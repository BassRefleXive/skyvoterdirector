<?php

declare(strict_types = 1);

namespace App\Component\User\Model;


use App\Component\User\Enum\UserRole;
use App\Component\User\Enum\UserType;

class Admin extends User
{
    protected const ROLES = [
        UserRole::ROLE_ADMIN,
    ];

    public function type(): UserType
    {
        return UserType::byValue(UserType::ADMIN);
    }
}