<?php

declare(strict_types=1);

namespace App\Component\Top\Model;

use App\Component\Top\Enum\ServerStatus;
use App\Component\User\Model\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\UuidInterface;

class Server
{
    private $id;
    private $top;
    private $providerId;
    private $title;
    private $status;
    private $jobs;
    private $owner;

    public function __construct(UuidInterface $uuid, User $owner, Top $top, string $providerId, string $title)
    {
        $this->id = $uuid;
        $this->owner = $owner;
        $this->top = $top;
        $this->providerId = $providerId;
        $this->title = $title;
        $this->status = ServerStatus::byValue(ServerStatus::ENABLED);
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function top(): Top
    {
        return $this->top;
    }

    public function providerId(): string
    {
        return $this->providerId;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function status(): ServerStatus
    {
        return $this->status;
    }

    public function enable(): self
    {
        if ($this->status()->isEnabled()) {
            throw new \LogicException(sprintf('Server "%s" already enabled.', $this->id()));
        }

        $this->status = ServerStatus::byValue(ServerStatus::ENABLED);

        return $this;
    }

    public function pause(): self
    {
        if ($this->status()->isPaused()) {
            throw new \LogicException(sprintf('Server "%s" already paused.', $this->id()));
        }

        $this->status = ServerStatus::byValue(ServerStatus::PAUSED);

        return $this;
    }

    public function disable(): self
    {
        if ($this->status()->isDisabled()) {
            throw new \LogicException(sprintf('Server "%s" already disabled.', $this->id()));
        }

        $this->status = ServerStatus::byValue(ServerStatus::DISABLED);

        return $this;
    }

    public function jobs(): Collection
    {
        null === $this->jobs && $this->jobs = new ArrayCollection();

        return $this->jobs;
    }

    public function owner(): User
    {
        return $this->owner;
    }
}
