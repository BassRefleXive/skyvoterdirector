<?php

declare(strict_types=1);

namespace App\Component\Top\Model\Builder;


use App\Component\CaptchaResolver\Model\CaptchaResolver;
use App\Component\Core\Exception\BuilderException;
use App\Component\Top\Enum\TopStatus;
use App\Component\Top\Model\Top;
use Ramsey\Uuid\UuidInterface;

class TopBuilder
{
    private $id;
    private $title;
    private $code;
    private $multiplier;
    private $status;
    private $captchaResolver;

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function withId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function withTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function withCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function multiplier(): float
    {
        return $this->multiplier;
    }

    public function withMultiplier(float $multiplier): self
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    public function status(): TopStatus
    {
        return $this->status;
    }

    public function withStatus(TopStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function captchaResolver(): CaptchaResolver
    {
        return $this->captchaResolver;
    }

    public function withCaptchaResolver(CaptchaResolver $captchaResolver): self
    {
        $this->captchaResolver = $captchaResolver;

        return $this;
    }

    public function build(): Top
    {
        $this->validate();

        return new Top($this);
    }

    private function validate(): void
    {
        foreach ($this as $property => $value) {
            if (null === $value) {
                throw BuilderException::missingProperty(Top::class, $property);
            }
        }
    }

}