<?php

declare(strict_types=1);

namespace App\Component\Top\Model;

use App\Component\CaptchaResolver\Model\CaptchaResolver;
use App\Component\Top\Enum\TopStatus;
use App\Component\Top\Model\Builder\TopBuilder;
use Ramsey\Uuid\UuidInterface;

class Top
{
    private $id;
    private $title;
    private $code;
    private $multiplier;
    private $status;
    private $servers;
    private $captchaResolver;

    public function __construct(TopBuilder $builder)
    {
        $this->id = $builder->id();
        $this->title = $builder->title();
        $this->code = $builder->code();
        $this->multiplier = $builder->multiplier();
        $this->status = $builder->status();
        $this->captchaResolver = $builder->captchaResolver();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function changeTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function changeCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function multiplier(): float
    {
        return $this->multiplier;
    }

    public function changeMultiplier(float $multiplier): self
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    public function status(): TopStatus
    {
        return $this->status;
    }

    public function changeStatus(TopStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function captchaResolver(): CaptchaResolver
    {
        return $this->captchaResolver;
    }

    public function changeCaptchaResolver(CaptchaResolver $captchaResolver): self
    {
        $this->captchaResolver = $captchaResolver;

        return $this;
    }
}
