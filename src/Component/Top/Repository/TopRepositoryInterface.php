<?php

declare(strict_types=1);

namespace App\Component\Top\Repository;


use App\Component\Core\Repository\RepositoryInterface;

interface TopRepositoryInterface extends RepositoryInterface
{

}