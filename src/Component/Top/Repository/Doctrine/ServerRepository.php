<?php

declare(strict_types = 1);

namespace App\Component\Top\Repository\Doctrine;

use App\Component\Admin\Criteria\ServerSearchCriteriaDto;
use App\Component\Core\Repository\Doctrine\BaseRepository;
use App\Component\Top\Model\Server;
use App\Component\Top\Repository\ServerRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class ServerRepository extends BaseRepository implements ServerRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Server::class);
    }

    public function findByUserId(UuidInterface $userId): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb->join('s.owner', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', (string) $userId);

        return $qb->getQuery()->getResult();
    }

    public function findByCriteria(ServerSearchCriteriaDto $criteriaDto): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb->select('s');

        if (null !== $criteriaDto->ids) {
            $qb->andWhere($qb->expr()->in('s.id', $criteriaDto->ids));
        }

        if (null !== $criteriaDto->ownerId) {
            $qb->join('s.owner', 'u')
                ->andWhere('u.id = :owner_id')
                ->setParameter('owner_id', $criteriaDto->ownerId);
        }

        return $qb->getQuery()->getResult();
    }
}
