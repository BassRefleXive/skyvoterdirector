<?php

declare(strict_types = 1);

namespace App\Component\Top\Repository\Doctrine;

use App\Component\Core\Repository\Doctrine\BaseRepository;
use App\Component\Top\Model\Top;
use App\Component\Top\Repository\TopRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

class TopRepository extends BaseRepository implements TopRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Top::class);
    }
}
