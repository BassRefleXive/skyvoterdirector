<?php

declare(strict_types = 1);

namespace App\Component\Top\Repository;


use App\Component\Admin\Criteria\ServerSearchCriteriaDto;
use App\Component\Core\Repository\RepositoryInterface;
use Ramsey\Uuid\UuidInterface;

interface ServerRepositoryInterface extends RepositoryInterface
{
    public function findByUserId(UuidInterface $userId): array;

    public function findByCriteria(ServerSearchCriteriaDto $criteriaDto): array;
}