<?php

declare(strict_types=1);

namespace App\Component\Top\Enum;


use MabeEnum\Enum;

final class TopStatus extends Enum
{
    public const ENABLED = 1;
    public const DISABLED = 0;
    public const PAUSED = 2;

    public function isEnabled(): bool
    {
        return $this->is(self::ENABLED);
    }

    public function isPaused(): bool
    {
        return $this->is(self::PAUSED);
    }

    public function isDisabled(): bool
    {
        return $this->is(self::DISABLED);
    }
}