<?php

declare(strict_types=1);

namespace App\Component\Top\Enum\Doctrine;

use App\Component\Top\Enum\ServerStatus;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ServerStatusType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ServerStatus
    {
        return null !== $value
            ? ServerStatus::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var ServerStatus $value */
        return null !== $value
            ? (string) $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'server_status_type';
    }
}