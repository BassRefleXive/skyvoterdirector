<?php

declare(strict_types=1);

namespace App\Component\Top\Enum\Doctrine;

use App\Component\Top\Enum\TopStatus;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class TopStatusType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?TopStatus
    {
        return null !== $value
            ? TopStatus::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var TopStatus $value */
        return null !== $value
            ? (string) $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'top_status_type';
    }
}