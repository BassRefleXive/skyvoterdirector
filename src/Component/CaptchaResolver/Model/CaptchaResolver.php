<?php

declare(strict_types=1);

namespace App\Component\CaptchaResolver\Model;

use Ramsey\Uuid\UuidInterface;

class CaptchaResolver
{
    private $id;
    private $code;
    private $key;
    private $tops;

    public function __construct(UuidInterface $uuid)
    {
        $this->id = $uuid;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function changeCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function key(): string
    {
        return $this->key;
    }

    public function changeKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }
}