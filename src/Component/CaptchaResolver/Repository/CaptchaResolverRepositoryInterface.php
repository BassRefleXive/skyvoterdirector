<?php

declare(strict_types=1);

namespace App\Component\CaptchaResolver\Repository;


use App\Component\Core\Repository\RepositoryInterface;

interface CaptchaResolverRepositoryInterface extends RepositoryInterface
{

}