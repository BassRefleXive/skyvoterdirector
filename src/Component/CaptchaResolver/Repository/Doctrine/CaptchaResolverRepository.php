<?php

declare(strict_types=1);

namespace App\Component\CaptchaResolver\Repository\Doctrine;


use App\Component\CaptchaResolver\Model\CaptchaResolver;
use App\Component\CaptchaResolver\Repository\CaptchaResolverRepositoryInterface;
use App\Component\Core\Repository\Doctrine\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CaptchaResolverRepository extends BaseRepository implements CaptchaResolverRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CaptchaResolver::class);
    }
}