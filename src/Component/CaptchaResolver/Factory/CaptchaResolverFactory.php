<?php

declare(strict_types=1);

namespace App\Component\CaptchaResolver\Factory;


use App\Component\CaptchaResolver\Model\CaptchaResolver;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\UuidInterface;

class CaptchaResolverFactory
{
    public function fromParams(UuidInterface $id, ParamFetcherInterface $params): CaptchaResolver
    {
        $captchaResolver = new CaptchaResolver($id);

        $captchaResolver->changeCode($params->get('code'))
            ->changeKey($params->get('key'));

        return $captchaResolver;
    }
}