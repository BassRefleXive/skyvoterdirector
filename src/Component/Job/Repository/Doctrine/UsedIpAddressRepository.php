<?php

declare(strict_types=1);

namespace App\Component\Job\Repository\Doctrine;


use App\Component\Core\Repository\Doctrine\BaseRepository;
use App\Component\Job\Model\UsedIpAddress;
use App\Component\Job\Repository\UsedIpAddressRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

class UsedIpAddressRepository extends BaseRepository implements UsedIpAddressRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsedIpAddress::class);
    }

    public function isUsed(string $address, \DateTime $date): bool
    {
        $qb = $this->createQueryBuilder('uia');

        $qb->select('COUNT(1)')
            ->where('uia.address = :address')
            ->andWhere('uia.date > :date')
            ->setParameter('address', $address)
            ->setParameter('date', $date->sub(new \DateInterval('PT12H'))->format('Y-m-d H:i:s'));

        return (bool) $qb->getQuery()->getSingleScalarResult();
    }
}