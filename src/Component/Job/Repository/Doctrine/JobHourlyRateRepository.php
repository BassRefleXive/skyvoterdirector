<?php
declare(strict_types=1);


namespace App\Component\Job\Repository\Doctrine;

use App\Component\Job\Model\JobHourlyRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class JobHourlyRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobHourlyRate::class);
    }

    public function getHourRate(int $hour): int
    {
        $qb = $this->createQueryBuilder('hr');

        $qb->select('COALESCE(SUM(hr.rate), 0)')
            ->where('hr.hour < :current_hour')
            ->setParameter('current_hour', $hour);

        return (int) $qb->getQuery()->getSingleScalarResult();
    }
}
