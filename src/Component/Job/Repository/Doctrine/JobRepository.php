<?php
declare(strict_types = 1);


namespace App\Component\Job\Repository\Doctrine;

use App\Component\Admin\Criteria\JobSearchCriteriaDto;
use App\Component\Core\Repository\Doctrine\BaseRepository;
use App\Component\Job\Enum\JobStatus;
use App\Component\Job\Model\Job;
use App\Component\Job\Repository\JobRepositoryInterface;
use App\Component\Top\Enum\TopStatus;
use App\Component\Top\Enum\ServerStatus;
use App\Component\User\Enum\Status as UserStatus;
use Doctrine\Common\Persistence\ManagerRegistry;

class JobRepository extends BaseRepository implements JobRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Job::class);
    }

    public function findCurrentJobs(float $hourRate): array
    {
        $now = new \DateTimeImmutable();

        $qb = $this->createQueryBuilder('j');

        $qb
            ->select('j, s, t')
            ->innerJoin('j.server', 's')
            ->innerJoin('s.owner', 'u')
            ->innerJoin('s.top', 't')
            ->where('j.date = :today')
            ->andWhere('j.status = :job_status_enabled')
            ->andWhere('t.status = :top_status_enabled')
            ->andWhere('u.status = :user_status_enabled')
            ->andWhere('s.status = :server_status_enabled')
            ->andWhere(sprintf('
                j.executedVotes + j.pendingVotes < 
                IF(
                    (:current_hour + 1) = 24,  
                    j.orderedVotes * t.multiplier, 
                    CEIL(j.orderedVotes * t.multiplier / 100 * :hourly_rate)
                )
            '))
            ->groupBy('j.server')
            ->addOrderBy('j.pendingVotes', 'ASC')
            ->addOrderBy('j.executedVotes', 'ASC')
            ->setParameter('job_status_enabled', JobStatus::ENABLED)
            ->setParameter('top_status_enabled', TopStatus::ENABLED)
            ->setParameter('user_status_enabled', UserStatus::ACTIVE)
            ->setParameter('server_status_enabled', ServerStatus::ENABLED)
            ->setParameter('today', $now->format('Y-m-d'))
            ->setParameter('current_hour', $now->format('H'))
            ->setParameter('hourly_rate', $hourRate);

        $data = $qb->getQuery()->getResult();

        return $data;
    }

    public function findByCriteria(JobSearchCriteriaDto $criteriaDto): array
    {
        $qb = $this->createQueryBuilder('j');

        $qb->select('j');

        if (null !== $criteriaDto->serverId) {
            $qb->join('j.server', 's')
                ->andWhere('s.id = :server_id')
                ->setParameter('server_id', $criteriaDto->serverId);
        }

        if (null !== $criteriaDto->period) {
            $qb->andWhere('j.date >= :start_date')
                ->andWhere('j.date <= :end_date')
                ->setParameter('start_date', $criteriaDto->period->getStartDate()->format('Y-m-d'))
                ->setParameter('end_date', $criteriaDto->period->getEndDate()->format('Y-m-d'));
        }

        if (null !== $criteriaDto->status) {
            $qb->andWhere('j.status = :job_status')->setParameter('job_status', $criteriaDto->status->getValue());
        }

        $qb->orderBy('j.date', 'ASC');

        return $qb->getQuery()->getResult();
    }
}
