<?php

declare(strict_types = 1);


namespace App\Component\Job\Repository;


use App\Component\Admin\Criteria\JobSearchCriteriaDto;
use App\Component\Core\Repository\RepositoryInterface;
use App\Component\Job\Model\Job;

interface JobRepositoryInterface extends RepositoryInterface
{
    /**
     * @param float $hourRate
     *
     * @return Job[]
     */
    public function findCurrentJobs(float $hourRate): array;

    /**
     * @return Job[]
     */
    public function findByCriteria(JobSearchCriteriaDto $criteriaDto): array;
}