<?php

declare(strict_types=1);

namespace App\Component\Job\Repository;

use App\Component\Core\Repository\RepositoryInterface;

interface UsedIpAddressRepositoryInterface extends RepositoryInterface
{
    public function isUsed(string $address, \DateTime $date): bool;
}