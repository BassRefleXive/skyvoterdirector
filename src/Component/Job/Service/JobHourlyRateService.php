<?php
declare(strict_types=1);


namespace App\Component\Job\Service;


use App\Component\Job\Repository\Doctrine\JobHourlyRateRepository;

class JobHourlyRateService
{
    private const HOURLY_RATE_RANDOMIZATION_PERCENT = 10;

    private $repository;

    public function __construct(JobHourlyRateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function hourRate(int $hour): float
    {
        $rate = $this->repository->getHourRate($hour);

        $rate = ($rate + mt_rand(-self::HOURLY_RATE_RANDOMIZATION_PERCENT, self::HOURLY_RATE_RANDOMIZATION_PERCENT)) / 10;

        return $rate;
    }
}
