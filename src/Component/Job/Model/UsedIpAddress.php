<?php

declare(strict_types=1);

namespace App\Component\Job\Model;


use App\Component\Core\Doctrine\Type\DateToString;

class UsedIpAddress
{
    private $address;
    private $date;

    public function __construct(string $address, DateToString $date)
    {
        $this->address = $address;
        $this->date = $date;
    }

    public function address(): string
    {
        return $this->address;
    }

    public function date(): DateToString
    {
        return $this->date;
    }
}