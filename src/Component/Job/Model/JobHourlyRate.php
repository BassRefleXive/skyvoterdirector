<?php
declare(strict_types = 1);


namespace App\Component\Job\Model;

class JobHourlyRate
{
    private $hour;
    private $rate;

    public function __construct(int $hour, int $rate)
    {
        $this->hour = $hour;
        $this->rate = $rate;
    }

    public function hour(): int
    {
        return $this->hour;
    }

    public function rate(): int
    {
        return $this->rate;
    }
}
