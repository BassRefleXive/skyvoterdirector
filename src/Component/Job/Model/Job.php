<?php
declare(strict_types = 1);


namespace App\Component\Job\Model;

use App\Component\Job\Enum\JobStatus;
use App\Component\Job\Model\Builder\JobBuilder;
use App\Component\Top\Model\Server;
use Ramsey\Uuid\UuidInterface;

class Job
{
    private $id;
    private $server;
    private $date;
    private $status;
    private $orderedVotes;
    private $pendingVotes;
    private $executedVotes;
    private $failedVotes;

    public function __construct(JobBuilder $builder)
    {
        $this->id = $builder->id();
        $this->server = $builder->server();
        $this->date = $builder->date();
        $this->status = $builder->status();
        $this->orderedVotes = $builder->orderedVotes();
        $this->pendingVotes = $builder->pendingVotes();
        $this->executedVotes = $builder->executedVotes();
        $this->failedVotes = $builder->failedVotes();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function server(): Server
    {
        return $this->server;
    }

    public function date(): \DateTimeInterface
    {
        return $this->date;
    }

    public function status(): JobStatus
    {
        return $this->status;
    }

    public function orderedVotes(): int
    {
        return $this->orderedVotes;
    }

    public function changeOrderedVotes(int $votes): self
    {
        $this->orderedVotes = $votes;

        return $this;
    }

    public function pendingVotes(): int
    {
        return $this->pendingVotes;
    }

    public function changePendingVotes(int $votes): self
    {
        $this->pendingVotes = $votes;

        return $this;
    }

    public function executedVotes(): int
    {
        return $this->executedVotes;
    }

    public function changeFailedVotes(int $votes): self
    {
        $this->failedVotes = $votes;

        return $this;
    }

    public function failedVotes(): int
    {
        return $this->failedVotes;
    }

    public function changeExecutedVotes(int $votes): self
    {
        $this->executedVotes = $votes;

        return $this;
    }

    public function enable(): self
    {
        if ($this->status()->isEnabled()) {
            throw new \LogicException(sprintf('Job "%s" already enabled.', $this->id()));
        }

        $this->status = JobStatus::byValue(JobStatus::ENABLED);

        return $this;
    }

    public function pause(): self
    {
        if ($this->status()->isPaused()) {
            throw new \LogicException(sprintf('Job "%s" already paused.', $this->id()));
        }

        $this->status = JobStatus::byValue(JobStatus::PAUSED);

        return $this;
    }

    public function disable(): self
    {
        if ($this->status()->isDisabled()) {
            throw new \LogicException(sprintf('Job "%s" already disabled.', $this->id()));
        }

        $this->status = JobStatus::byValue(JobStatus::DISABLED);

        return $this;
    }

    public function executeVote(): void
    {
        if ($this->pendingVotes < 1) {
            throw new \LogicException(sprintf('Tried to execute job "%s" which is not in execution state.', $this->id));
        }

        $this->executedVotes++;
        $this->pendingVotes--;
    }

    public function failVote(): void
    {
        $this->failedVotes++;
        $this->pendingVotes--;
    }

    public function queueVote(): void
    {
        $this->pendingVotes++;
    }
}
