<?php
declare(strict_types = 1);


namespace App\Component\Job\Model\Builder;

use App\Component\Core\Exception\BuilderException;
use App\Component\Job\Enum\JobStatus;
use App\Component\Job\Model\Job;
use App\Component\Top\Model\Server;
use Ramsey\Uuid\UuidInterface;

class JobBuilder
{
    private $id;
    private $server;
    private $date;
    private $status;
    private $orderedVotes;
    private $pendingVotes;
    private $executedVotes;
    private $failedVotes;

    public function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function withServer(Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function server(): Server
    {
        return $this->server;
    }

    public function withDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function date(): \DateTime
    {
        return $this->date;
    }

    public function withStatus(JobStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function status(): JobStatus
    {
        return $this->status;
    }

    public function withOrderedVotes(int $count): self
    {
        $this->orderedVotes = $count;

        return $this;
    }

    public function orderedVotes(): int
    {
        return $this->orderedVotes;
    }

    public function withPendingVotes(int $count): self
    {
        $this->pendingVotes = $count;

        return $this;
    }

    public function pendingVotes(): int
    {
        return $this->pendingVotes;
    }

    public function withExecutedVotes(int $count): self
    {
        $this->executedVotes = $count;

        return $this;
    }

    public function executedVotes(): int
    {
        return $this->executedVotes;
    }

    public function withFailedVotes(int $count): self
    {
        $this->failedVotes = $count;

        return $this;
    }

    public function failedVotes(): int
    {
        return $this->failedVotes;
    }

    public function build(): Job
    {
        $this->validate();

        return new Job($this);
    }

    private function validate(): void
    {
        foreach ($this as $property => $value) {
            if (null === $value) {
                throw BuilderException::missingProperty(Job::class, $property);
            }
        }
    }
}
