<?php

declare(strict_types = 1);

namespace App\Component\Job\Enum\Doctrine;

use App\Component\Job\Enum\JobStatus;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class JobStatusType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?JobStatus
    {
        return null !== $value
            ? JobStatus::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var JobStatus $value */
        return null !== $value
            ? (string) $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'job_status_type';
    }
}