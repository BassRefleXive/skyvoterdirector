<?php
declare(strict_types = 1);


namespace App\Component\Worker\Job;


use App\Component\Job\Model\Job;
use App\Component\Job\Repository\Doctrine\JobRepository;
use App\Component\Job\Service\JobHourlyRateService;
use Ramsey\Uuid\UuidInterface;

class JobService
{
    private $jobRepository;
    private $jobHourlyRateService;

    public function __construct(JobRepository $jobRepository, JobHourlyRateService $jobHourlyRateService)
    {
        $this->jobRepository = $jobRepository;
        $this->jobHourlyRateService = $jobHourlyRateService;
    }

    public function retrieveNextJobs(): array
    {
        $now = new \DateTimeImmutable();

        $rate = $this->jobHourlyRateService->hourRate((int) $now->format('H'));
        $jobs = $this->jobRepository->findCurrentJobs($rate);

        $jobs = $this->filterUniqueServerProviders($jobs);

        foreach ($jobs as $job) {
            $job->queueVote();
        }

        $this->jobRepository->bulkSave($jobs);

        return $jobs;
    }

    public function execute(UuidInterface $id): void
    {
        /** @var Job $job */
        $job = $this->jobRepository->getById($id);

        $job->executeVote();

        $this->jobRepository->save($job);
    }

    public function fail(UuidInterface $id): void
    {
        /** @var Job $job */
        $job = $this->jobRepository->getById($id);

        $job->failVote();

        $this->jobRepository->save($job);
    }

    /**
     * @param Job[] $jobs
     *
     * @return Job[]
     */
    private function filterUniqueServerProviders(array $jobs): array
    {
        $results = [];

        foreach ($jobs as $job) {
            $found = false;

            /** @var Job $result */
            foreach ($results as $result) {
                if ($result->server()->top()->id()->equals($job->server()->top()->id())) {
                    $found = true;

                    break;
                }
            }

            if ($found) {
                continue;
            }

            $results[] = $job;
        }

        return $results;
    }
}
