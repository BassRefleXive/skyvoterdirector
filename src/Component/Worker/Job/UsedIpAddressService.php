<?php

declare(strict_types=1);

namespace App\Component\Worker\Job;


use App\Component\Core\Doctrine\Type\DateToString;
use App\Component\Job\Model\UsedIpAddress;
use App\Component\Job\Repository\UsedIpAddressRepositoryInterface;

class UsedIpAddressService
{
    private $usedIpAddressRepository;

    public function __construct(UsedIpAddressRepositoryInterface $usedIpAddressRepository)
    {
        $this->usedIpAddressRepository = $usedIpAddressRepository;
    }

    public function check(string $address): bool
    {
        $result = $this->usedIpAddressRepository->isUsed($address, new \DateTime());

        if (!$result) {
            $address = new UsedIpAddress($address, new DateToString());

            $this->usedIpAddressRepository->save($address);
        }

        return $result;
    }
}