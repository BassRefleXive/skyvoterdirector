<?php

declare(strict_types=1);

namespace App\Component\Core\Repository\Doctrine;


use App\Component\Core\Exception\MissingEntityException;
use App\Component\Core\Repository\RepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class BaseRepository extends ServiceEntityRepository implements RepositoryInterface
{
    public final function nextIdentity(): UuidInterface
    {
        return Uuid::uuid4();
    }

    public function save($entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }

    public function remove(UuidInterface $id): void
    {
        $entity = $this->_em->getReference($this->_entityName, $id);

        $this->_em->remove($entity);
        $this->_em->flush($entity);
    }

    public function getById(UuidInterface $id)
    {
        $entity = $this->find($id);

        if (null === $entity) {
            throw MissingEntityException::missingById($id, $this->_entityName);
        }

        return $entity;
    }

    public function all(): array
    {
        $qb = $this->_em->getConnection()->createQueryBuilder();

        return $qb->select('p.ip, p.port')
            ->from('proxy', 'p')
            ->execute()
            ->fetchAll();
    }

    public function today(): array
    {
        $qb = $this->_em->getConnection()->createQueryBuilder();

        return $qb->select('p.ip, p.port')
            ->from('proxy', 'p')
            ->where('p.created_at > :today')
            ->setParameter('today', (new \DateTime())->format('Y-m-d'))
            ->execute()
            ->fetchAll();
    }

    public function findByIds(array $ids): array
    {
        return $this->findBy(['id' => $ids]);
    }

    public function bulkSave(array $jobs): void
    {
        foreach ($jobs as $job) {
            $this->_em->persist($job);
        }

        $this->_em->flush();
    }
}