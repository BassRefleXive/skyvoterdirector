<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Assembler;


use App\Component\Core\Http\Factory\UriFactory;
use App\Component\Core\Http\Criteria\RequestCriteria;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

abstract class RequestAssembler
{
    private $uriFactory;

    public function __construct(UriFactory $uriFactory)
    {
        $this->uriFactory = $uriFactory;
    }

    public function fromCriteria(RequestCriteria $criteria): RequestInterface
    {
        return new Request(
            $criteria->method(),
            $this->uriFactory->create($criteria->uriCriteria()),
            $criteria->headers(),
            $criteria->body()
        );
    }
}