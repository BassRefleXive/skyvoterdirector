<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Exception;


class ResponseException extends \RuntimeException
{
    public static function invalidFormat(string $expectedFormat, string $message): self
    {
        return new self(sprintf('Invalid response format. Expected "%s". Message: "%s"', $expectedFormat, $message));
    }

    public static function missingIndex(string $index): self
    {
        return new self(sprintf('Missing "%s" index in response.', $index));
    }
}