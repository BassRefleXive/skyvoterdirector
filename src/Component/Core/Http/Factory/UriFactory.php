<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Factory;


use App\Component\Core\Http\Criteria\UriCriteria;
use GuzzleHttp\Psr7\Uri;

abstract class UriFactory
{
    public function create(UriCriteria $uriCriteria): string
    {
        $uri = new Uri($this->domain());

        return (string) $uri
            ->withPath(implode('', array_merge(
                [$uri->getPath()],
                mb_substr($uriCriteria->path(), 0, 1) !== '/'
                    ? ['/']
                    : [],
                [$uriCriteria->path()]
            )))
            ->withQuery($uriCriteria->query());
    }

    abstract protected function domain(): string;
}