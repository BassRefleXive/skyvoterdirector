<?php

declare(strict_types=1);

namespace App\Component\Core\Doctrine\Type;


class DateToString extends \DateTime
{
    public function __toString()
    {
        return $this->format('Y-m-d');
    }
}