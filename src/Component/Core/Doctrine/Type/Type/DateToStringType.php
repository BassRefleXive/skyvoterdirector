<?php

declare(strict_types=1);

namespace App\Component\Core\Doctrine\Type\Type;


use App\Component\Core\Doctrine\Type\DateToString;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateTimeType;

class DateToStringType extends DateTimeType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $dateTime = parent::convertToPHPValue($value, $platform);

        if (!$dateTime) {
            return $dateTime;
        }

        return new DateToString($dateTime->format('Y-m-d'));
    }

    public function getName()
    {
        return 'date_to_string_type';
    }
}