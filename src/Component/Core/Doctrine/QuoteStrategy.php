<?php

declare(strict_types=1);

namespace App\Component\Core\Doctrine;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\ORM\Mapping\AnsiQuoteStrategy;
use Doctrine\ORM\Mapping\ClassMetadata;

class QuoteStrategy extends AnsiQuoteStrategy
{
    private function quote($token, AbstractPlatform $platform)
    {
        switch ($platform->getName()) {
            case 'mysql':
            default:
                return '`' . $token . '`';
        }
    }

    public function getColumnName($fieldName, ClassMetadata $class, AbstractPlatform $platform)
    {
        return $this->quote($class->fieldMappings[$fieldName]['columnName'], $platform);
    }
}