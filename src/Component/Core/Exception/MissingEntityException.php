<?php
declare(strict_types = 1);


namespace App\Component\Core\Exception;

class MissingEntityException extends \RuntimeException
{
    public static function missingById($id, string $class): self
    {
        return new self(
            sprintf(
                'Entity "%s" with id "%s" not found.',
                $class,
                $id
            )
        );
    }
}
