<?php
declare(strict_types = 1);


namespace App\Component\Core\Exception;

class BuilderException extends \RuntimeException
{
    public static function missingProperty(string $class, string $property): self
    {
        return new self(
            sprintf(
                'Property "%s" must be set and must be not null when building instance of "%s".',
                $property,
                $class
            )
        );
    }
}
