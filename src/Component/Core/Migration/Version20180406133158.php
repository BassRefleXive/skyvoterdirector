<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180406133158 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', email VARCHAR(128) NOT NULL, password VARCHAR(512) NOT NULL, type_id integer NOT NULL COMMENT \'(DC2Type:user_type)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE = InnoDB');

        $this->addSql('INSERT INTO `user` (`id`, `email`, `password`, `type_id`) SELECT `id`, `email`, "123qwe", 1 FROM `client`');

        $this->addSql('DROP TABLE client');
        $this->addSql('ALTER TABLE server ADD user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', DROP client_id');
        $this->addSql('ALTER TABLE server ADD CONSTRAINT FK_5A6DD5F6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_5A6DD5F6A76ED395 ON server (user_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE server DROP FOREIGN KEY FK_5A6DD5F6A76ED395');
        $this->addSql('CREATE TABLE client (id CHAR(36) NOT NULL COLLATE utf8_general_ci COMMENT \'(DC2Type:uuid)\', email VARCHAR(128) NOT NULL COLLATE utf8_general_ci, UNIQUE INDEX UNIQ_C7440455E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('INSERT INTO `client` (`id`, `email`) SELECT `id`, `email` FROM `user`');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP INDEX IDX_5A6DD5F6A76ED395 ON server');
        $this->addSql('ALTER TABLE server ADD client_id CHAR(36) NOT NULL COLLATE utf8_general_ci COMMENT \'(DC2Type:uuid)\', DROP user_id');
    }
}
