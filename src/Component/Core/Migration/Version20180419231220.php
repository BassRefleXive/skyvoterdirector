<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use App\Component\Proxy\Enum\ProxyTypeStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180419231220 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proxy_type (`id` CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', `code` VARCHAR(32) NOT NULL, `status` integer NOT NULL COMMENT \'(DC2Type:proxy_type_status_type)\', PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE = InnoDB');

        $this->addSql(sprintf('INSERT INTO `proxy_type` (`id`, `code`, `status`) VALUES ("%s", "%s", "%d")', (string) Uuid::uuid4(), 'tor', ProxyTypeStatus::ENABLED));
        $this->addSql(sprintf('INSERT INTO `proxy_type` (`id`, `code`, `status`) VALUES ("%s", "%s", "%d")', (string) Uuid::uuid4(), 'proxy_list', ProxyTypeStatus::DISABLED));
        $this->addSql(sprintf('INSERT INTO `proxy_type` (`id`, `code`, `status`) VALUES ("%s", "%s", "%d")', (string) Uuid::uuid4(), 'rotating', ProxyTypeStatus::DISABLED));
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE proxy_type');
    }
}
