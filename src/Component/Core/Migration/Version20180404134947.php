<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180404134947 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `top` ADD COLUMN `captcha_resolver_id` char(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\';');
        $this->addSql('ALTER TABLE top ADD INDEX IDX_1ED91FCACD09CFC1 (captcha_resolver_id)');

        $this->addSql('UPDATE `top` SET `captcha_resolver_id`=(SELECT `id` FROM `captcha_resolver` WHERE `code`="two_captcha")');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
