<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180409180117 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job ADD `status` integer NOT NULL COMMENT \'(DC2Type:job_status_type)\'');
        $this->addSql('ALTER TABLE server RENAME INDEX idx_5a6dd5f64584665a TO IDX_5A6DD5F6C82CB256');
        $this->addSql('ALTER TABLE top ADD CONSTRAINT FK_1ED91FCACD09CFC1 FOREIGN KEY (captcha_resolver_id) REFERENCES captcha_resolver (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job DROP `status`');
        $this->addSql('ALTER TABLE server RENAME INDEX idx_5a6dd5f6c82cb256 TO IDX_5A6DD5F64584665A');
        $this->addSql('ALTER TABLE top DROP FOREIGN KEY FK_1ED91FCACD09CFC1');
    }
}
