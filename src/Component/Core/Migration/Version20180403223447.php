<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180403223447 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (0, 31)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (1, 16)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (2, 14)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (3, 11)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (4, 12)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (5, 18)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (6, 22)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (7, 28)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (8, 43)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (9, 49)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (10, 41)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (11, 37)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (12, 32)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (13, 28)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (14, 30)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (15, 38)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (16, 45)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (17, 57)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (18, 76)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (19, 105)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (20, 92)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (21, 73)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (22, 57)');
        $this->addSql('INSERT INTO `job_hourly_rate` (`hour`, `rate`) VALUES (23, 45)');

        $this->connection->executeQuery('INSERT INTO `client` (`id`, `email`) VALUES (:id, :email)', ['id' => (string) Uuid::uuid4(), 'email' => 'mihails.bagrovs@gmail.com']);
        $this->connection->executeQuery('INSERT INTO `client` (`id`, `email`) VALUES (:id, :email)', ['id' => (string) Uuid::uuid4(), 'email' => 'support@prime.mu']);

        $stmt = $this->connection->prepare(
            'INSERT INTO `server` (`id`, `client_id`, `top_id`, `provider_id`) VALUES (
                :id, 
                (SELECT `id` FROM `client` WHERE `email` = :email_addr), 
                (SELECT `id` FROM `top` WHERE `code` = :top_code), 
                :provider_id
                )'
        );

        $serverIds = [
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
            (string) Uuid::uuid4(),
        ];

        $stmt->execute(['id' => $serverIds[0], 'email_addr' => 'mihails.bagrovs@gmail.com', 'top_code' => 'mpogtop', 'provider_id' => 'mpogtop_id_1']);
        $stmt->execute(['id' => $serverIds[1], 'email_addr' => 'support@prime.mu', 'top_code' => 'mpogtop', 'provider_id' => 'mpogtop_id_2']);
        $stmt->execute(['id' => $serverIds[2], 'email_addr' => 'mihails.bagrovs@gmail.com', 'top_code' => 'gamestop100', 'provider_id' => 'gamestop100_id_1']);
        $stmt->execute(['id' => $serverIds[3], 'email_addr' => 'support@prime.mu', 'top_code' => 'gamestop100', 'provider_id' => 'gamestop100_id_2']);
        $stmt->execute(['id' => $serverIds[4], 'email_addr' => 'mihails.bagrovs@gmail.com', 'top_code' => 'top100arena', 'provider_id' => 'top100arena_id_1']);
        $stmt->execute(['id' => $serverIds[5], 'email_addr' => 'support@prime.mu', 'top_code' => 'top100arena', 'provider_id' => 'top100arena_id_2']);
        $stmt->execute(['id' => $serverIds[6], 'email_addr' => 'mihails.bagrovs@gmail.com', 'top_code' => 'xtremetop100', 'provider_id' => 'xtremetop100_id_1']);
        $stmt->execute(['id' => $serverIds[7], 'email_addr' => 'support@prime.mu', 'top_code' => 'xtremetop100', 'provider_id' => 'xtremetop100_id_2']);
        $stmt->execute(['id' => $serverIds[8], 'email_addr' => 'mihails.bagrovs@gmail.com', 'top_code' => 'topservers200', 'provider_id' => 'topservers200_id_1']);
        $stmt->execute(['id' => $serverIds[9], 'email_addr' => 'support@prime.mu', 'top_code' => 'topservers200', 'provider_id' => 'topservers200_id_2']);

        $stmt = $this->connection->prepare('INSERT INTO `job` (`id`, `server_id`, `date`, `ordered_votes`, `pending_votes`, `executed_votes`) VALUES (:id, :server_id, :date, :ordered, :pending, :executed)');

        foreach ($serverIds as $serverId) {
            $stmt->execute([
                'id'        => (string) Uuid::uuid4(),
                'server_id' => $serverId,
                'date'      => (new \DateTimeImmutable())->format('Y-m-d'),
                'ordered'   => 0,
                'pending'   => 0,
                'executed'  => 0,
            ]);
        }
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM `job_hourly_rate`');
        $this->addSql('DELETE FROM `job`');
        $this->addSql('DELETE FROM `server`');
    }
}
