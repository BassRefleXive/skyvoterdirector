<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180404134155 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `captcha_resolver` (`id` char(36) NOT NULL COMMENT \'(DC2Type:uuid)\',`code` varchar(32) NOT NULL,`key` varchar(32) NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

//        $this->addSql('ALTER TABLE server DROP FOREIGN KEY FK_5A6DD5F6C82CB256');
//        $this->addSql('DROP INDEX FK_5A6DD5F6C82CB256 ON server');
        $this->addSql('ALTER TABLE server CHANGE top_id top_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE server ADD CONSTRAINT FK_5A6DD5F64584665A FOREIGN KEY (top_id) REFERENCES top (id)');
        $this->addSql('CREATE INDEX IDX_5A6DD5F64584665A ON server (top_id)');
        $this->addSql('ALTER TABLE job CHANGE server_id server_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F81844E6B7 FOREIGN KEY (server_id) REFERENCES server (id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F81844E6B7 ON job (server_id)');

        $this->addSql(sprintf('INSERT INTO `captcha_resolver` (`id`, `code`, `key`) VALUES ("%s", "%s", "%s")', (string) Uuid::uuid4(), 'two_captcha', '487a1c1390b36cb7b6b184b711fac84d'));
        $this->addSql(sprintf('INSERT INTO `captcha_resolver` (`id`, `code`, `key`) VALUES ("%s", "%s", "%s")', (string) Uuid::uuid4(), 'anti_captcha', '25a294878e3fb0238efbcc2c7206e2d4'));
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F81844E6B7');
        $this->addSql('DROP INDEX IDX_FBD8E0F81844E6B7 ON job');
        $this->addSql('ALTER TABLE job CHANGE server_id server_id CHAR(36) NOT NULL COLLATE utf8_general_ci COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE server DROP FOREIGN KEY FK_5A6DD5F64584665A');
        $this->addSql('DROP INDEX IDX_5A6DD5F64584665A ON server');
        $this->addSql('ALTER TABLE server CHANGE top_id top_id CHAR(36) DEFAULT NULL COLLATE utf8_general_ci COMMENT \'(DC2Type:uuid)\'');
    }
}
