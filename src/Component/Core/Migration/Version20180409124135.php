<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180409124135 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE top RENAME INDEX uniq_1ed91fca2b36786b TO UNIQ_1ED91FCAE2E33245');
        $this->addSql('ALTER TABLE top RENAME INDEX uniq_1ed91fca77153098 TO UNIQ_1ED91FCA1E07D977');
        $this->addSql('ALTER TABLE user ADD `status` integer NOT NULL COMMENT \'(DC2Type:user_status_type)\'');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_8d93d649e7927c74 TO UNIQ_8D93D6496F279BB4');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE top RENAME INDEX uniq_1ed91fcae2e33245 TO UNIQ_1ED91FCA2B36786B');
        $this->addSql('ALTER TABLE top RENAME INDEX uniq_1ed91fca1e07d977 TO UNIQ_1ED91FCA77153098');
        $this->addSql('ALTER TABLE user DROP `status`');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_8d93d6496f279bb4 TO UNIQ_8D93D649E7927C74');
    }
}
