<?php declare(strict_types = 1);

namespace App\Component\Core\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180403182947 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(sprintf('INSERT INTO `top` (`id`, `title`, `code`) VALUES ("%s", "%s", "%s")', (string) Uuid::uuid4(), 'XTremeTop100', 'xtremetop100'));
        $this->addSql(sprintf('INSERT INTO `top` (`id`, `title`, `code`) VALUES ("%s", "%s", "%s")', (string) Uuid::uuid4(), 'GamesTop100', 'gamestop100'));
        $this->addSql(sprintf('INSERT INTO `top` (`id`, `title`, `code`) VALUES ("%s", "%s", "%s")', (string) Uuid::uuid4(), 'MpoGTop', 'mpogtop'));
        $this->addSql(sprintf('INSERT INTO `top` (`id`, `title`, `code`) VALUES ("%s", "%s", "%s")', (string) Uuid::uuid4(), 'TopServers200', 'topservers200'));
        $this->addSql(sprintf('INSERT INTO `top` (`id`, `title`, `code`) VALUES ("%s", "%s", "%s")', (string) Uuid::uuid4(), 'Top100Arena', 'top100arena'));
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE * FROM `top`');
    }
}
