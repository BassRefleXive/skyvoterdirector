<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Service;


use App\Component\Core\Http\Exception\ResponseException;
use App\Component\Proxy\Exception\MissingProxyException;
use App\Component\Proxy\Exception\ProxyDiscoveryException;
use App\Component\Proxy\Gateway\Service\ProxyGateway;
use App\Component\Proxy\Model\Proxy;
use App\Component\Proxy\Repository\ProxyRepositoryInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class Discoverer
{
    private $gateway;
    private $proxyRepository;

    public function __construct(ProxyGateway $gateway, ProxyRepositoryInterface $proxyRepository)
    {
        $this->gateway = $gateway;
        $this->proxyRepository = $proxyRepository;
    }

    public function one(): void
    {
        try {
            $proxy = $this->gateway->next();

            if (!$proxy->shouldBeStored()) {
                return;
            }

            $match = $this->proxyRepository->getByIpAndPort($proxy->ip(), $proxy->port());

            if ($proxy->lastChecked() > $match->lastChecked()) {
                $match->rechecked($proxy->lastChecked());

                $this->proxyRepository->save($match);
            }
        } catch (MissingProxyException $e) {
            $proxyEntity = new Proxy($proxy->ip(), $proxy->port(), $proxy->type(), $proxy->lastChecked());

            $this->proxyRepository->save($proxyEntity);
        } catch (ResponseException $e) {
            throw ProxyDiscoveryException::responseException($e);
        }

        $this->proxyRepository->clear();
    }

    public function multiple(): int
    {
        $discovered = 0;

        try {
            $proxies = $this->gateway->list();

            foreach ($proxies as $proxy) {
                if (!$proxy->shouldBeStored()) {
                    continue;
                }

                try {
                    $match = $this->proxyRepository->getByIpAndPort($proxy->ip(), $proxy->port());

                    if ($proxy->lastChecked() > $match->lastChecked()) {
                        $match->rechecked($proxy->lastChecked());

                        $this->proxyRepository->save($match);
                    }
                } catch (MissingProxyException $e) {
                    $proxyEntity = new Proxy($proxy->ip(), $proxy->port(), $proxy->type(), $proxy->lastChecked());

                    $this->proxyRepository->save($proxyEntity);

                    ++$discovered;
                } catch (UniqueConstraintViolationException $e) {

                }
            }

        } catch (ResponseException $e) {
            throw ProxyDiscoveryException::responseException($e);
        }

        $this->proxyRepository->clear();

        return $discovered;
    }
}