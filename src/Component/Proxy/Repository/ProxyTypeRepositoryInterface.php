<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Repository;

use App\Component\Core\Repository\RepositoryInterface;
use App\Component\Proxy\Model\ProxyType;

interface ProxyTypeRepositoryInterface extends RepositoryInterface
{
    public function getEnabled(): ProxyType;
}