<?php

declare(strict_types=1);

namespace App\Component\Proxy\Repository;

use App\Component\Core\Repository\RepositoryInterface;
use App\Component\Proxy\Model\Proxy;

interface ProxyRepositoryInterface extends RepositoryInterface
{
    public function getByIpAndPort(string $ip, int $port): Proxy;
}