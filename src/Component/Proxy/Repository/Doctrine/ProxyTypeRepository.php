<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Repository\Doctrine;


use App\Component\Proxy\Enum\ProxyTypeStatus;
use App\Component\Proxy\Model\ProxyType;
use App\Component\Proxy\Repository\ProxyTypeRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Component\Core\Repository\Doctrine\BaseRepository;

class ProxyTypeRepository extends BaseRepository implements ProxyTypeRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProxyType::class);
    }

    public function getEnabled(): ProxyType
    {
        return $this->findOneBy(['status' => ProxyTypeStatus::ENABLED]);
    }
}