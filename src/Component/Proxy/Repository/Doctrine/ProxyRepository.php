<?php

declare(strict_types=1);

namespace App\Component\Proxy\Repository\Doctrine;


use App\Component\Proxy\Exception\MissingProxyException;
use App\Component\Proxy\Model\Proxy;
use App\Component\Proxy\Repository\ProxyRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Component\Core\Repository\Doctrine\BaseRepository;

class ProxyRepository extends BaseRepository implements ProxyRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Proxy::class);
    }

    public function getByIpAndPort(string $ip, int $port): Proxy
    {
        $qb = $this->createQueryBuilder('p');

        $qb->select('p')
            ->where('p.ip = :ip')
            ->andWhere('p.port = :port')
            ->setParameter('ip', $ip)
            ->setParameter('port', $port);

        /** @var Proxy|null $proxy */
        $proxy = $qb->getQuery()->getOneOrNullResult();

        if (null === $proxy) {
            throw MissingProxyException::missingByIpAndPort($ip, $port);
        }

        return $proxy;
    }
}