<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Model;


use App\Component\Proxy\Enum\ProxyTypeStatus;
use Ramsey\Uuid\UuidInterface;

class ProxyType
{
    private $id;
    private $code;
    private $status;

    public function __construct(UuidInterface $uuid)
    {
        $this->id = $uuid;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function status(): ProxyTypeStatus
    {
        return $this->status;
    }
}