<?php

declare(strict_types=1);

namespace App\Component\Proxy\Model;


class Proxy
{
    private $ip;
    private $port;
    private $type;
    private $lastChecked;
    private $createdAt;
    private $updatedAt;

    public function __construct(string $ip, int $port, string $type, \DateTimeInterface $lastChecked)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->type = $type;
        $this->lastChecked = $lastChecked;
        $this->createdAt = $this->updatedAt = new \DateTime();
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function port(): int
    {
        return $this->port;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function lastChecked(): \DateTimeInterface
    {
        return $this->lastChecked;
    }

    public function createdAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function updatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function connectionString(): string
    {
        return sprintf('%s:%d', $this->ip, $this->port);
    }

    public function rechecked(\DateTimeInterface $date): void
    {
        $this->lastChecked = $date;

        $this->updatedAt = new \DateTime();
    }
}