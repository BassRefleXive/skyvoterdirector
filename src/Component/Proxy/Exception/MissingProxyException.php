<?php

declare(strict_types=1);

namespace App\Component\Proxy\Exception;


use App\Component\Core\Exception\MissingEntityException;

class MissingProxyException extends MissingEntityException
{
    public static final function missingByIpAndPort(string $ip, int $port): self
    {
        return new self(
            sprintf(
                'Proxy with IP "%s" and port %d not found.',
                $ip,
                $port
            )
        );
    }
}