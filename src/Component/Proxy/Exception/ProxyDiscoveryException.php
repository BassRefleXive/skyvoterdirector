<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Exception;


class ProxyDiscoveryException extends \RuntimeException
{
    public static final function responseException(\Throwable $e): self
    {
        throw new self('Failed to discover proxy. Gateway returned error.', $e->getCode(), $e);
    }
}