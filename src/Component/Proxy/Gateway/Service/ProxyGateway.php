<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Gateway\Service;


use App\Component\Core\Http\Assembler\RequestAssembler;
use App\Component\Core\Http\Criteria\RequestCriteria;
use App\Component\Core\Http\HttpClientInterface;
use App\Component\Proxy\Gateway\Criteria\ProxyIssuerUriCriteria;
use App\Component\Proxy\Gateway\Factory\ResponseFactory;
use App\Component\Proxy\Gateway\Model\Builder\ProxyBuilder;
use App\Component\Proxy\Gateway\Model\Proxy;
use Symfony\Component\HttpFoundation\Request;

class ProxyGateway
{
    private $client;
    private $requestAssembler;
    private $proxyBuilder;
    private $responseFactory;

    public function __construct(HttpClientInterface $client, RequestAssembler $requestAssembler)
    {
        $this->client = $client;
        $this->requestAssembler = $requestAssembler;
        $this->proxyBuilder = new ProxyBuilder();
        $this->responseFactory = new ResponseFactory();
    }

    public function next(): Proxy
    {
        $requestCriteria = new RequestCriteria(
            ProxyIssuerUriCriteria::next(),
            Request::METHOD_GET
        );

        return $this->proxyBuilder->fromArray(
            $this->responseFactory->next(
                $this->client->send($this->requestAssembler->fromCriteria($requestCriteria))
            )
        );
    }

    /**
     * @return Proxy[]
     */
    public function list(): array
    {
        $requestCriteria = new RequestCriteria(
            ProxyIssuerUriCriteria::list(),
            Request::METHOD_GET
        );

        $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

        return array_map([$this->proxyBuilder, 'fromArray'], $this->responseFactory->list($response));
    }
}