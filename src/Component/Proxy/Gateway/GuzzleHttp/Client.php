<?php

declare(strict_types=1);

namespace App\Component\Proxy\Gateway\GuzzleHttp;

use App\Component\Core\Http\GuzzleHttp\Client as BaseClient;

class Client extends BaseClient
{
}