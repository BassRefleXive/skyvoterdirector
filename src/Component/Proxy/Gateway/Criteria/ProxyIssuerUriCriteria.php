<?php

declare(strict_types=1);

namespace App\Component\Proxy\Gateway\Criteria;


use App\Component\Core\Http\Criteria\UriCriteria;

class ProxyIssuerUriCriteria extends UriCriteria
{
    public static function next(): self
    {
        return new self(
            '/',
            [],
            [
                'apiKey' => 'SUrFZdX5vPoc7yaVgT8HCBDhQLMAfe4n',
                'get' => true,
                'post' => true,
                'cookies' => true,
                'userAgent' => true,
                'referer' => true,
            ]
        );
    }

    public static function list(): self
    {
        return new self(
            'proxy-list',
            [],
            [
                'apiKey' => 'SUrFZdX5vPoc7yaVgT8HCBDhQLMAfe4n',
            ]
        );
    }
}