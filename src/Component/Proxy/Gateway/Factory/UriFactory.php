<?php

declare(strict_types=1);

namespace App\Component\Proxy\Gateway\Factory;

use App\Component\Core\Http\Factory\UriFactory as BaseUriFactory;

class UriFactory extends BaseUriFactory
{
    private $domain;

    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }

    protected function domain(): string
    {
        return $this->domain;
    }
}