<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Gateway\Factory;


use App\Component\Core\Http\Exception\ResponseException;
use Psr\Http\Message\ResponseInterface;

class ResponseFactory
{
    public function list(ResponseInterface $response): array
    {
        $contents = $response->getBody()->getContents();
        $result = [];
        foreach (explode(PHP_EOL, $contents) as $proxyData) {
            $proxyParts = explode(':', $proxyData);

            if (2 === count($proxyParts)) {
                $result[] = [
                    'ip'             => $proxyParts[0],
                    'port'           => $proxyParts[1],
                    'connectionType' => 'unknown',
                    'type'           => 'unknown',
                    'lastChecked'    => time(),
                    'get'            => true,
                    'post'           => true,
                    'cookies'        => true,
                    'referer'        => true,
                    'userAgent'      => true,
                    'city'           => 'unknown',
                    'state'          => 'unknown',
                    'country'        => 'unknown',
                ];
            }
        }

        return $result;
    }

    public function next(ResponseInterface $response): array
    {
        $contents = $response->getBody()->getContents();

        if (null === $data = json_decode($contents, true)) {
            throw ResponseException::invalidFormat('json', $contents);
        }

        $this->checkValueExistence($data, [
            'ip', 'port', 'connectionType', 'type',
            'lastChecked', 'get', 'post',
            'cookies', 'referer', 'userAgent',
            'city', 'state', 'country',
        ]);

        return $data;
    }

    private function checkValueExistence(array $array, array $indexes)
    {
        array_map(function (string $index) use ($array) {
            if (!isset($array[$index])) {
                throw ResponseException::missingIndex($index);
            }
        }, $indexes);
    }
}