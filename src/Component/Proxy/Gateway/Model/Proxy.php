<?php

declare(strict_types=1);

namespace App\Component\Proxy\Gateway\Model;


use App\Component\Proxy\Gateway\Model\Builder\ProxyBuilder;

class Proxy
{
    private $ip;
    private $port;
    private $connectionType;
    private $type;
    private $lastChecked;
    private $get;
    private $post;
    private $cookies;
    private $referer;
    private $userAgent;
    private $city;
    private $state;
    private $country;

    public function __construct(ProxyBuilder $builder)
    {
        $this->ip = $builder->ip();
        $this->port = $builder->port();
        $this->connectionType = $builder->connectionType();
        $this->type = $builder->type();
        $this->lastChecked = $builder->lastChecked();
        $this->get = $builder->isGet();
        $this->post = $builder->isPost();
        $this->cookies = $builder->supportsCookies();
        $this->referer = $builder->supportsReferer();
        $this->userAgent = $builder->supportsUserAgent();
        $this->city = $builder->city();
        $this->state = $builder->state();
        $this->country = $builder->country();
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function port(): int
    {
        return $this->port;
    }

    public function connectionType(): string
    {
        return $this->connectionType;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function lastChecked(): \DateTimeInterface
    {
        return $this->lastChecked;
    }

    public function isGet(): bool
    {
        return $this->get;
    }

    public function isPost(): bool
    {
        return $this->post;
    }

    public function supportsCookies(): bool
    {
        return $this->cookies;
    }

    public function supportsReferer(): bool
    {
        return $this->referer;
    }

    public function supportsUserAgent(): bool
    {
        return $this->userAgent;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function state(): string
    {
        return $this->state;
    }

    public function country(): string
    {
        return $this->country;
    }

    public function shouldBeStored(): bool
    {
        return 1 === preg_match(
                '/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',
                $this->ip()
            );
    }
}