<?php

declare(strict_types=1);

namespace App\Component\Proxy\Gateway\Model\Builder;


use App\Component\Core\Exception\BuilderException;
use App\Component\Core\Http\Exception\ResponseException;
use App\Component\Proxy\Gateway\Model\Proxy;
use Psr\Http\Message\ResponseInterface;

class ProxyBuilder
{
    private $ip;
    private $port;
    private $connectionType;
    private $type;
    private $lastChecked;
    private $get;
    private $post;
    private $cookies;
    private $referer;
    private $userAgent;
    private $city;
    private $state;
    private $country;

    public function withIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function withPort(int $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function port(): int
    {
        return $this->port;
    }

    public function withConnectionType(string $connectionType): self
    {
        $this->connectionType = $connectionType;

        return $this;
    }

    public function connectionType(): string
    {
        return $this->connectionType;
    }

    public function withType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function withLastChecked(\DateTimeInterface $lastChecked): self
    {
        $this->lastChecked = $lastChecked;

        return $this;
    }

    public function lastChecked(): \DateTimeInterface
    {
        return $this->lastChecked;
    }

    public function withGet(bool $get): self
    {
        $this->get = $get;

        return $this;
    }

    public function isGet(): bool
    {
        return $this->get;
    }

    public function withPost(bool $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function isPost(): bool
    {
        return $this->post;
    }

    public function withCookies(bool $cookies): self
    {
        $this->cookies = $cookies;

        return $this;
    }

    public function supportsCookies(): bool
    {
        return $this->cookies;
    }

    public function withReferer(bool $referer): self
    {
        $this->referer = $referer;

        return $this;
    }

    public function supportsReferer(): bool
    {
        return $this->referer;
    }

    public function withUserAgent(bool $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function supportsUserAgent(): bool
    {
        return $this->userAgent;
    }

    public function withCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function withState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function state(): string
    {
        return $this->state;
    }

    public function withCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function country(): string
    {
        return $this->country;
    }

    public function fromArray(array $data): Proxy
    {
        return (new self())
            ->withIp($data['ip'])
            ->withPort((int) $data['port'])
            ->withConnectionType($data['connectionType'])
            ->withType($data['type'])
            ->withLastChecked((new \DateTime())->setTimestamp($data['lastChecked']))
            ->withGet((bool) $data['get'])
            ->withPost((bool) $data['post'])
            ->withCookies((bool) $data['cookies'])
            ->withReferer((bool) $data['referer'])
            ->withUserAgent((bool) $data['userAgent'])
            ->withCity($data['city'])
            ->withState($data['state'])
            ->withCountry($data['country'])
            ->build();
    }

    public function build(): Proxy
    {
        $this->validate();

        return new Proxy($this);
    }

    private function validate(): void
    {
        foreach ($this as $property => $value) {
            if (null === $value) {
                throw BuilderException::missingProperty(Proxy::class, $property);
            }
        }
    }
}