<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Enum;

use MabeEnum\Enum;

final class ProxyTypeStatus extends Enum
{
    public const ENABLED = 1;
    public const DISABLED = 0;

    public function isEnabled(): bool
    {
        return $this->is(self::ENABLED);
    }

    public function isDisabled(): bool
    {
        return $this->is(self::DISABLED);
    }
}