<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Enum\Doctrine;

use App\Component\Proxy\Enum\ProxyTypeStatus;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ProxyTypeStatusType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ProxyTypeStatus
    {
        return null !== $value
            ? ProxyTypeStatus::byValue((int) $value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        if (null === $value) {
            return null;
        }

        if ($value instanceof ProxyTypeStatus) {
            return $value->getValue();
        }

        return (int) $value;
    }

    public function getName(): string
    {
        return 'proxy_type_status_type';
    }
}