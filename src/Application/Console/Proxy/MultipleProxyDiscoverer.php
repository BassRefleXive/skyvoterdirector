<?php

declare(strict_types = 1);

namespace App\Application\Console\Proxy;


use App\Component\Proxy\Service\Discoverer;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class MultipleProxyDiscoverer extends Command
{
    private $discoverer;
    private $logger;

    public function __construct(Discoverer $discoverer, LoggerInterface $logger)
    {
        parent::__construct();

        $this->discoverer = $discoverer;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('app:proxy:proxy-discoverer:multiple');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Discovering New Proxies');

        while (true) {
            $stopwatch = new Stopwatch();
            $stopwatch->start($this->getName());

            $count = $this->discoverer->multiple();

            $event = $stopwatch->stop($this->getName());

            $io->newLine();
            $io->success(
                sprintf(
                    'Discovered %d proxies in %.3f seconds, %.3f MB memory used.',
                    $count,
                    $event->getDuration() / 1000,
                    $event->getMemory() / 1024 / 1024
                )
            );

            sleep(360); // Perform requests each 6 minutes
        }
    }
}