<?php

declare(strict_types=1);

namespace App\Application\Console\Proxy;

use App\Component\Admin\Service\JobService;
use League\Period\Period;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class IncreaseOrderedVotesCountCommand extends Command
{
    private $jobService;

    public function __construct(JobService $jobService)
    {
        parent::__construct();

        $this->jobService = $jobService;
    }

    protected function configure()
    {
        $this->setName('app:job:increase-ordered-votes')
            ->addArgument('select-start-date', InputArgument::REQUIRED)
            ->addArgument('select-end-date', InputArgument::REQUIRED)
            ->addArgument('multiplier', InputArgument::REQUIRED)
            ->addArgument('server-id', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Increase Ordered Votes Count');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $this->jobService->changeOrderedVotesCount(
            new Period(
                new \DateTimeImmutable($input->getArgument('select-start-date')),
                new \DateTimeImmutable($input->getArgument('select-end-date'))
            ),
            (float) $input->getArgument('multiplier'),
            null !== $input->getArgument('server-id')
                ? Uuid::fromString($input->getArgument('server-id'))
                : null
        );

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}