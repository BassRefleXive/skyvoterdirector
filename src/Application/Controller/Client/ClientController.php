<?php

declare(strict_types=1);

namespace App\Application\Controller\Client;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;


/**
 * @FOSRest\Route("/client")
 */
class ClientController
{
    /**
     * @FOSRest\Get("/")
     */
    public function indexAction(): View
    {
        return View::create('Hello, World!');
    }
}