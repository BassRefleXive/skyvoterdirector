<?php

declare(strict_types=1);

namespace App\Application\Controller\Admin;

use App\Component\Admin\Service\CaptchaResolverService;
use App\Component\CaptchaResolver\Repository\CaptchaResolverRepositoryInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;


/**
 * @FOSRest\Route("/admin/captcha-resolver")
 */
class CaptchaResolverController
{
    private $captchaResolverRepository;

    public function __construct(CaptchaResolverRepositoryInterface $captchaResolverRepository)
    {
        $this->captchaResolverRepository = $captchaResolverRepository;
    }

    /**
     * @FOSRest\Get("/")
     */
    public function listAction(): View
    {
        return View::create($this->captchaResolverRepository->all())->setContext(
            (new Context())
                ->addGroup('captcha-resolver')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Get("/{id}")
     */
    public function getAction(string $id): View
    {
        return View::create($this->captchaResolverRepository->getById(Uuid::fromString($id)))->setContext(
            (new Context())
                ->addGroup('captcha-resolver')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="code", description="Code")
     * @FOSRest\RequestParam(name="key", description="Key")
     */
    public function createAction(CaptchaResolverService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->create($params), Response::HTTP_CREATED)->setContext(
            (new Context())
                ->addGroup('captcha-resolver')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Put("/{id}")
     *
     * @FOSRest\RequestParam(name="code", description="Code", default=null, nullable=true)
     * @FOSRest\RequestParam(name="key", description="Key", default=null, nullable=true)
     */
    public function updateAction(string $id, CaptchaResolverService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->update(Uuid::fromString($id), $params))->setContext(
            (new Context())
                ->addGroup('captcha-resolver')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Delete("/{id}")
     */
    public function removeAction(string $id): View
    {
        $this->captchaResolverRepository->remove(Uuid::fromString($id));

        return View::create(null);
    }
}