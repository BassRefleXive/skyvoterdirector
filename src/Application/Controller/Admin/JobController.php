<?php

declare(strict_types = 1);

namespace App\Application\Controller\Admin;

use App\Component\Admin\Criteria\Factory\JobSearchCriteriaDtoFactory;
use App\Component\Admin\Service\JobService;
use App\Component\Admin\Service\ServerService;
use App\Component\Job\Enum\JobStatus;
use App\Component\Job\Repository\JobRepositoryInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FOSRest\Route("/admin/job")
 */
class JobController
{
    private $jobRepository;

    public function __construct(JobRepositoryInterface $jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    /**
     * @FOSRest\Get("/find")
     *
     * @FOSRest\QueryParam(name="server_id", description="Server ID", default=null, nullable=true)
     * @FOSRest\QueryParam(name="start_date", description="Start Date", default=null, nullable=true)
     * @FOSRest\QueryParam(name="end_date", description="End Date", default=null, nullable=true)
     * @FOSRest\QueryParam(name="status", description="Status", default=null, nullable=true)
     */
    public function findAction(ParamFetcherInterface $params): View
    {
        return View::create(
            $this->jobRepository->findByCriteria(
                (new JobSearchCriteriaDtoFactory())->fromParams($params)
            )
        )->setContext(
            (new Context())
                ->enableMaxDepth()
                ->addGroup('job')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Get("/{id}")
     */
    public function getAction(string $id): View
    {
        return View::create($this->jobRepository->getById(Uuid::fromString($id)))->setContext(
            (new Context())
                ->enableMaxDepth()
                ->addGroup('job')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="jobs", description="Jobs Array")
     * @FOSRest\RequestParam(name="server_id", description="Server ID")
     */
    public function createAction(JobService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->create($params), Response::HTTP_CREATED)->setContext(
            (new Context())
                ->enableMaxDepth()
                ->addGroup('job')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Put("/{id}")
     *
     * @FOSRest\RequestParam(name="ordered_votes", description="Ordered Votes Count", default=null, nullable=true)
     * @FOSRest\RequestParam(name="pending_votes", description="Pending Votes Count", default=null, nullable=true)
     * @FOSRest\RequestParam(name="executed_votes", description="Executed Votes Count", default=null, nullable=true)
     * @FOSRest\RequestParam(name="status", description="Status", default=null, nullable=true)
     */
    public function updateAction(string $id, JobService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->update(Uuid::fromString($id), $params))->setContext(
            (new Context())
                ->enableMaxDepth()
                ->addGroup('job')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Delete("/{id}")
     *
     */
    public function removeAction(string $id): View
    {
        $this->jobRepository->remove(Uuid::fromString($id));

        return View::create(null);
    }

    /**
     * @FOSRest\Get("/meta/statuses")
     */
    public function statusesAction(): View
    {
        return View::create(JobStatus::getNames());
    }
}