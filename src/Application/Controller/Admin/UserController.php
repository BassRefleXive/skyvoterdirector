<?php

declare(strict_types=1);

namespace App\Application\Controller\Admin;


use App\Component\Admin\Service\UserService;
use App\Component\User\Enum\Status;
use App\Component\User\Enum\UserType;
use App\Component\User\Repository\UserRepositoryInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FOSRest\Route("/admin/user")
 */
class UserController
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @FOSRest\Get("/")
     */
    public function listAction(): View
    {
        return View::create($this->userRepository->all())->setContext(
            (new Context())
                ->addGroup('user')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Get("/{id}")
     */
    public function getAction(string $id): View
    {
        return View::create($this->userRepository->getById(Uuid::fromString($id)))->setContext(
            (new Context())
                ->addGroup('user')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="email", description="Email")
     * @FOSRest\RequestParam(name="password", description="Password")
     * @FOSRest\RequestParam(name="type", description="Type")
     */
    public function createAction(UserService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->create($params))->setContext(
            (new Context())
                ->addGroup('user')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Put("/{id}")
     *
     * @FOSRest\RequestParam(name="status", description="Status", default=null, nullable=true)
     */
    public function updateAction(string $id, UserService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->update(Uuid::fromString($id), $params))->setContext(
            (new Context())
                ->addGroup('user')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }
}