<?php

declare(strict_types = 1);

namespace App\Application\Controller\Admin;


use App\Component\Admin\Criteria\Factory\ServerSearchCriteriaDtoFactory;
use App\Component\Admin\Service\ServerService;
use App\Component\Top\Enum\ServerStatus;
use App\Component\Top\Repository\ServerRepositoryInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FOSRest\Route("/admin/server")
 */
class ServerController
{
    private $serverRepository;

    public function __construct(ServerRepositoryInterface $serverRepository)
    {
        $this->serverRepository = $serverRepository;
    }

    /**
     * @FOSRest\Get("/find")
     *
     * @FOSRest\QueryParam(name="ids", description="Server IDs", default=null, nullable=true)
     * @FOSRest\QueryParam(name="owner_id", description="Owner ID", default=null, nullable=true)
     */
    public function findAction(ParamFetcherInterface $params): View
    {
        return View::create($this->serverRepository->findByCriteria(
            (new ServerSearchCriteriaDtoFactory())->fromParams($params))
        )->setContext(
            (new Context())
                ->addGroup('server')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Get("/{id}")
     */
    public function getAction(string $id): View
    {
        return View::create($this->serverRepository->getById(Uuid::fromString($id)))->setContext(
            (new Context())
                ->addGroup('server')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="title", description="Title")
     * @FOSRest\RequestParam(name="provider_id", description="Provider ID")
     * @FOSRest\RequestParam(name="top_id", description="Top ID")
     * @FOSRest\RequestParam(name="owner_id", description="Owner ID")
     */
    public function createAction(ServerService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->create($params), Response::HTTP_CREATED)->setContext(
            (new Context())
                ->addGroup('server')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Put("/{id}")
     *
     * @FOSRest\RequestParam(name="status", description="Status", default=null, nullable=true)
     */
    public function updateAction(string $id, ServerService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->update(Uuid::fromString($id), $params))->setContext(
            (new Context())
                ->addGroup('server')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Delete("/{id}")
     *
     */
    public function removeAction(string $id): View
    {
        $this->serverRepository->remove(Uuid::fromString($id));

        return View::create(null);
    }

    /**
     * @FOSRest\Get("/meta/statuses")
     */
    public function statusesAction(): View
    {
        return View::create(ServerStatus::getNames());
    }
}