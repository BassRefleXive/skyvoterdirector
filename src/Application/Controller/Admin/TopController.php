<?php
declare(strict_types=1);


namespace App\Application\Controller\Admin;

use App\Component\Admin\Service\TopService;
use App\Component\Top\Enum\TopStatus;
use App\Component\Top\Repository\TopRepositoryInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FOSRest\Route("/admin/top")
 */
class TopController
{
    private $topRepository;

    public function __construct(TopRepositoryInterface $topRepository)
    {
        $this->topRepository = $topRepository;
    }

    /**
     * @FOSRest\Get("/")
     */
    public function listAction(): View
    {
        return View::create($this->topRepository->all())->setContext(
            (new Context())
                ->addGroup('top')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Get("/{id}")
     */
    public function getAction(string $id): View
    {
        return View::create($this->topRepository->getById(Uuid::fromString($id)))->setContext(
            (new Context())
                ->addGroup('top')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="title", description="Title")
     * @FOSRest\RequestParam(name="code", description="Code")
     * @FOSRest\RequestParam(name="multiplier", description="Multiplier")
     * @FOSRest\RequestParam(name="captcha_resolver", description="Multiplier")
     */
    public function createAction(TopService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->create($params), Response::HTTP_CREATED)->setContext(
            (new Context())
                ->addGroup('top')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Put("/{id}")
     *
     * @FOSRest\RequestParam(name="title", description="Title", default=null, nullable=true)
     * @FOSRest\RequestParam(name="code", description="Code", default=null, nullable=true)
     * @FOSRest\RequestParam(name="multiplier", description="Multiplier", default=null, nullable=true)
     * @FOSRest\RequestParam(name="captcha_resolver", description="Multiplier", default=null, nullable=true)
     * @FOSRest\RequestParam(name="status", description="Status", default=null, nullable=true)
     */
    public function updateAction(string $id, TopService $service, ParamFetcherInterface $params): View
    {
        return View::create($service->update(Uuid::fromString($id), $params))->setContext(
            (new Context())
                ->addGroup('top')
                ->addGroup('relation')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Delete("/{id}")
     */
    public function removeAction(string $id): View
    {
        $this->topRepository->remove(Uuid::fromString($id));

        return View::create(null);
    }

    /**
     * @FOSRest\Get("/meta/statuses")
     */
    public function statusesAction(): View
    {
        return View::create(TopStatus::getNames());
    }
}
