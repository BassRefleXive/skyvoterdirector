<?php
declare(strict_types = 1);


namespace App\Application\Controller\Worker;

use App\Component\Proxy\Repository\ProxyTypeRepositoryInterface;
use App\Component\Worker\Job\JobService;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FOSRest\Route("/worker/job")
 */
class JobController
{
    private $jobService;

    public function __construct(JobService $jobService)
    {
        $this->jobService = $jobService;
    }

    /**
     * @FOSRest\Get("/next")
     */
    public function currentAction(ProxyTypeRepositoryInterface $proxyTypeRepository): View
    {
        return View::create($this->jobService->retrieveNextJobs())->setContext(
            (new Context())
                ->addGroup('worker')
                ->addGroup('id')
        );
    }

    /**
     * @FOSRest\Put("/{id}/execute")
     */
    public function executeAction(string $id): View
    {
        $this->jobService->execute(Uuid::fromString($id));

        return View::create(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @FOSRest\Put("/{id}/fail")
     */
    public function failAction(string $id): View
    {
        $this->jobService->fail(Uuid::fromString($id));

        return View::create(null, Response::HTTP_ACCEPTED);
    }
}
