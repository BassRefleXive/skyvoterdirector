<?php

declare(strict_types=1);

namespace App\Application\Controller\Worker;


use App\Component\Worker\Job\UsedIpAddressService;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

/**
 * @FOSRest\Route("/worker/ip-address")
 */
class UsedIpAddressController
{
    /**
     * @FOSRest\Get("/check")
     *
     * @FOSRest\QueryParam(name="address", description="IP Address", nullable=false)
     */
    public function currentAction(UsedIpAddressService $service, ParamFetcherInterface $params): View
    {
        return View::create([
            'used' => $service->check($params->get('address'))
        ]);
    }
}