<?php

declare(strict_types = 1);

namespace App\Application\Controller\Worker;

use App\Component\Proxy\Repository\ProxyRepositoryInterface;
use App\Component\Proxy\Repository\ProxyTypeRepositoryInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;

/**
 * @FOSRest\Route("/worker/proxy")
 */
class ProxyController
{
    /**
     * @FOSRest\Get("/list")
     */
    public function currentAction(ProxyRepositoryInterface $proxyRepository): View
    {
        return View::create($proxyRepository->all());
    }
    /**
     * @FOSRest\Get("/today")
     */
    public function todayAction(ProxyRepositoryInterface $proxyRepository): View
    {
        return View::create($proxyRepository->today());
    }

    /**
     * @FOSRest\Get("/type")
     */
    public function typeAction(ProxyTypeRepositoryInterface $proxyTypeRepository): View
    {
        return View::create($proxyTypeRepository->getEnabled())->setContext(
            (new Context())
                ->addGroup('id')
                ->addGroup('worker')
        );
    }
}