<?php

declare(strict_types = 1);

namespace App\Application\Controller;

use App\Component\User\Enum\Status;
use App\Component\User\Enum\UserType;
use App\Component\User\Repository\UserRepositoryInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @FOSRest\Route("/user")
 */
class UserController
{
    private $authenticator;
    private $manager;
    private $userRepository;

    public function __construct(JWTTokenAuthenticator $authenticator, JWTManager $manager, UserRepositoryInterface $userRepository)
    {
        $this->authenticator = $authenticator;
        $this->manager = $manager;
        $this->userRepository = $userRepository;
    }

    /**
     * @FOSRest\Get("/me")
     */
    public function meAction(Request $request): View
    {
        $credentials = $this->authenticator->getCredentials($request);

        if (!$credentials instanceof TokenInterface) {
            return View::create(null, Response::HTTP_NOT_FOUND);
        }

        $data = $this->manager->decode($credentials);

        if (!is_array($data)) {
            return View::create(null, Response::HTTP_NOT_FOUND);
        }

        return View::create($this->userRepository->getByEmail($data['username']))->setContext(
            (new Context())->addGroup('user')
        );
    }

    /**
     * @FOSRest\Get("/types")
     */
    public function typesAction(): View
    {
        return View::create(UserType::getNames());
    }

    /**
     * @FOSRest\Get("/statuses")
     */
    public function statusesAction(): View
    {
        return View::create(Status::getNames());
    }
}