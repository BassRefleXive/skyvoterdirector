<?php

declare(strict_types=1);

namespace App\Application\Controller;

use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

class ExceptionController
{
    public function showAction(): View
    {
        return View::create(['error' => 'An error has been occurred'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
