<?php

declare(strict_types=1);

namespace App\Application\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof ExpiredTokenException) {
            return;
        }

        if ($exception instanceof InvalidTokenException) {
            return;
        }

        $this->logger->error(
            'Unhandled general exception {exception}: {error}',
            [
                'exception' => get_class($exception),
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]
        );
    }
}
